#!/usr/bin/env python3
import sys
import pylab as pl
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
from matplotlib import colors
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)

if len(sys.argv) < 2:
    print('tpd.py [topdrawer file]')
    
inputfile = str(sys.argv[1])

# check if string is a number
def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

# function to read the topdrawer files and return the set of histograms
def readtdfile(infile):
    infile_read = open(infile, 'r')
    numhisto = -1
    all_data_X = []
    all_data_Y = []
    all_data_err = []
    all_title_top = []
    all_title_left = []
    all_title_bottom = []
    all_limit_X = []
    all_limit_Y = []
    all_log_X = []
    all_log_Y = []
    hist_data_X = []
    hist_data_Y = []
    hist_data_err = []
    hist_limit_X = []
    hist_limit_Y = []
    hist_log_X = []
    hist_log_Y = []
    hist_title_top = ""
    hist_title_left = ""
    hist_title_bottom = ""
    for line in infile_read:
        if "NEW FRAME" in line: # start new histogram
            print("NEW HISTO")
            numhisto = numhisto + 1
            hist_log_X.append(0)
            hist_log_Y.append(0)
            #print("Found next histo")
        if "TITLE TOP" in line:
            hist_title_top = ' '.join(line.split()[2:])
            print("TITLE TOP=", hist_title_top)
        if "TITLE LEFT" in line:
            hist_title_left =' '.join(line.split()[2:])
            #print("TITLE LEFT=", ' '.join(hist_title_left[numhisto]))
        if "TITLE BOTTOM" in line:
            hist_title_bottom = ' '.join(line.split()[2:])
            #print("TITLE BOTTOM=", ' '.join(hist_title_bottom[numhisto]))
        if "SET SCALE Y LOG" in line:
            hist_log_Y[numhisto] = 1
        if "SET SCALE X LOG" in line:
            hist_log_X[numhisto] = 1
        if "SET LIMITS X" in line:
            hist_limit_X = [float(line.split()[3]), float(line.split()[4])]
            #print("SET LIMITS X", hist_limit_X[numhisto])
        #print(line.split()[0], "line.split()[0].isnumeric()=", line.split()[0].isnumeric())
        if is_number(line.split()[0]):
            #print("appending X=", float(line.split()[0]))
            hist_data_X.append(float(line.split()[0]))
            hist_data_Y.append(float(line.split()[1]))
            if(len(line.split()) > 2):
                hist_data_err.append(float(line.split()[2]))
        if "HIST" in line: # end histogram
            all_data_X.append(hist_data_X)
            all_data_Y.append(hist_data_Y)
            all_data_err.append(hist_data_err)
            all_title_top.append(hist_title_top)
            all_title_left.append(hist_title_left)
            all_title_bottom.append(hist_title_bottom)
            all_limit_X.append(hist_limit_X)
            all_limit_Y.append(hist_limit_Y)
            all_log_X.append(hist_log_X)
            all_log_Y.append(hist_log_Y)
            hist_data_X = []
            hist_data_Y = []
            hist_data_err = []
            hist_limit_X = []
            hist_limit_Y = []
            hist_log_X = []
            hist_log_Y = []
            hist_title_top = ""
            hist_title_left = ""
            hist_title_bottom = ""
    infile_read.close()
    return all_data_X, all_data_Y, all_data_err, all_title_top, all_title_left, all_title_bottom, all_limit_X, all_limit_Y, all_log_X, all_log_Y


def plottd(all_data_X, all_data_Y, all_data_err, all_title_top, all_title_left, all_title_bottom, all_limit_X, all_limit_Y, all_log_X, all_log_Y):
    pdf = PdfPages(inputfile.replace('.top','.pdf'))
    for i in range(len(all_data_X)):
        print(i,all_title_top[i],all_data_X[i],all_data_Y[i])
        plt.clf()
        fig, ax = plt.subplots(figsize=(20, 10))
        plt.step(all_data_X[i], all_data_Y[i], linestyle=None, marker='.', lw=4, ms=0,where='mid')
        plt.title(all_title_top[i], loc='center')
        print(all_limit_X[i])
        if len(all_limit_X[i]) > 1:
            plt.xlim([all_limit_X[i][0],all_limit_X[i][1]])
        if len(all_limit_Y[i]) > 1:
            plt.ylim([all_limit_Y[i][0],all_limit_Y[i][1]])
        pdf.savefig(fig)
        
    pdf.close()
    
##################################
print('Reading', inputfile)

# read the input file
all_data_X, all_data_Y, all_data_err, all_title_top, all_title_left, all_title_bottom, all_limit_X, all_limit_Y, all_log_X, all_log_Y = readtdfile(inputfile)

# plot the data
plottd(all_data_X, all_data_Y, all_data_err, all_title_top, all_title_left, all_title_bottom, all_limit_X, all_limit_Y, all_log_X, all_log_Y)

